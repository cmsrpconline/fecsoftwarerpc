Here are my notes for compilation for CC7

 

In config/FecRules.linux

 

# Define for the compilation

FECSOFT_C++FLAGS   += -fPIC -DDCUADDRESS=${DCUADDRESS} ${FECSOFTDEBUG} ${HAL_C++FLAGS} -Wno-error=unused-but-set-variable

 

 

Below are earlier instructions  - probably should be applied first

 

And as far as I remember when we moved from one version of linux to another we had to do some (a few) changes in the codes, otherwise it was not possible to compile, or it was compiled but then was crashing.

 

Karol

 

From: Karol Bunkowski 
Sent: Thursday, March 20, 2014 6:04 PM
Subject: RE: FecSoftware instructions reminder

 

Hi

So the version of the software which we use in the P5 and which is working on the SLC6 and with the A3818 is in this directory in the P5:

/nfshome0/rpcpro/FecSoftwareV3_0

It contains a few small but important changes with respect to the version we took from

http://cmstracker029.cern.ch/newsoft/FecSoftwareV3_0.tgz (in any case this PC is not working now)

 

The compilation instructions are as follows:

 

Modify paths accordingly in

config/cvs_profile

 

in directory FecSoftwareV3_0

ln -s config/cvs_profile

./configure   --with-oracle-path=no --with-fed9u-path=no --with-trkonline-path=no  --with-diag-path=no

Or (depends on the PC setup):

./configure --with-oracle-path=no --with-fed9u-path=no --with-trkonline-path=no --with-diag-path=no --with-xerces-path=/opt/xdaq/lib/ --with-caen-path=/opt/xdaq/lib

                                                                                                                                                                                          

The output should look more or less like that:

---------------------------------------------------------

enable XDAQ rpmbuild : yes (from default)

enable USBFec        : no (from default)

enable PCIFec        : no (from default)

enable SBS           : no (from default)

XDAQ root            : /opt/xdaq (from environment)

XDAQ OS              : linux (from environment)

XDAQ platform        : x86_64_slc5 (from environment)

HAL root             : /var/opt/xdaq/hal (from environment)

Oracle home          : no (from cmd-line)

Xerces-c root        : /opt/xdaq/lib (from environment)

Fed9USoftware  path  :  (from cmd-line)

CAEN software root   : /opt/xdaq/lib (from environment)

TrackerOnline path   :  (from cmd-line)

DiagSystem path      :  (from cmd-line)

---------------------------------------------------------

configure: creating ./config.status

config.status: creating FecHeader.linux

config.status: creating Makefile

---------------------------------------------------------

 

Then

make Generic                 

make APIConsoleDebugger

(for ProgramTest.exe)

 

in /FecSoftwareV3_0/config

ln -s FecAddressTable0_7.dat FecAddressTable.dat

 

the needed library should be then in:

/nfshome0/rpcpro/FecSoftwareV3_0/generic/lib/linux/x86_64_slc5/libDeviceAccess.so

 

Regards
